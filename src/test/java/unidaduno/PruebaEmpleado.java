package unidaduno;

import java.util.Scanner;

public class PruebaEmpleado {

    private static String nombre;
    private static String apellido;
    private static double salario;
    private static Scanner entrada;
    private static int empleados;

    private static void ingresarEmpleado(int i){
        entrada = new Scanner(System.in);
        System.out.print("\nSolicitud y recepci�n de los datos del empleado " + (i));

        // Nombre
        do{
            System.out.print("\nEscriba su nombre: ");
            nombre = entrada.nextLine();
        }while(nombre.equals(""));

        // Apellido
        do{
            System.out.print("Escriba su apellido: ");
            apellido = entrada.nextLine();
        }while(apellido.equals(""));

        // Salario
        do {
            System.out.print("Escriba su salario mensual, recuerda que debe ser un n�mero positivo y mayor a 0: ");
            while (!entrada.hasNextDouble()) {
                System.out.printf("\"%s\" no es un n�mero.\nEscriba su salario mensual: ", entrada.next());
            }
            salario = entrada.nextDouble();
        } while (salario < 1.0);

        // Muestra el resumen del empleado
        Empleado empleado = new Empleado(nombre,apellido,salario);
        System.out.print("Resumen para empleado " + (i) +": ");
        empleado.mostrarResumen();
        System.out.println("\n******************************************************************\n");
    }

    private static void ingresarNumeroEmpleados(){
        entrada = new Scanner(System.in);
        empleados = 0;
        while (empleados <= 0){
            System.out.print("�Cu�ntos empleados quiere ingresar? (debe ser un n�mero mayor a 0): ");
            while (!entrada.hasNextInt()) {
                System.out.printf("\"%s\" no es un n�mero.\nFavor de ingresar el n�mero de empleados (debe ser un n�mero mayor a 0): ",
                        entrada.next());
            }
            empleados = entrada.nextInt();
        }
    }

    // El m�todo main inicia y finaliza la ejecuci�n de la aplicaci�n
    public static void main(String args[]) {
        System.out.println("\n****************** BIENVENIDO AL SISTEMA DE C�CULO SALARIAL DE EMPLEADOS **************************\n");
        ingresarNumeroEmpleados();

        for (int i=1; i <= empleados; i++){
            ingresarEmpleado(i);
        }
    }
}