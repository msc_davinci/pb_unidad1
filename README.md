# Unidad 1: Programación Básica

## **1. Introducción**

Ejercicio a resolver:

Desarrolla una aplicación que permita resolver el siguiente ejercicio. Recuerda incluir las instrucciones y la pantalla de salida.

Cree una clase llamada Empleado, que incluya tres piezas de información como variables de instancia: un primer nombre (tipo String), un apellido paterno (tipo String) y un salario mensual (double). Su clase debe tener un constructor que inicialice las tres variables de instancia. Proporcione un método establecer y un método obtener para cada variable de instancia. Escriba una aplicación de prueba llamada PruebaEmpleado, que demuestre las capacidades de cada Empleado. Cree dos objetos Empleado y muestre el salario anual de cada objeto. Después, proporcione a cada Empleado un aumento del 10% y muestre el salario anual de cada Empleado otra vez.

Requerimientos necesarios en tu computadora:
- Permisos de administrador para descargar e intalar software.
- Chocolatey package manager (sólo en Windows y es opcional).
- Homebrew (MAC y es opcional).
- Java JDK >1.8.
- Maven 3.5.
- IntelliJ, Eclipse Luna o cualquier otro IDE para java.

## **2. Herramientas**

Los lenguajes y lenguaje de programación utilizados en este projecto son:

1. Java como lenguaje de programación.
2. Maven como herramienta para manejo de dependencias.
3. Git software de control de versiones.

## **3. Instalación de Herramientas**

Si tu equipo ya cuenta con Java 1.8 o mayor y Maven 3.4 o mayor, puedes omitir esta sección.

**Windows**
**Instalación de Chocolatey**
Visita la página https://chocolatey.org/install y sigue los pasos de instalación.

**Instalación de Java**
1. Abre una linea de comandos como administrador.
2. Copia y pega en terminal lo siguiente: ```choco install jdk8 -y```.
3. Espera a que se completen los comandos. 
4. Si no vez algún error ejecuta el comando ```refreshenv```. 
5. Listo para usar Java .

**Instalación de Maven**
1. Abre una linea de comandos como administrador.
2. Copia y pega en terminal lo siguiente: ```choco install maven -y```.
3. Espera a que se completen los comandos. 
4. Si no vez algún error ejecuta el comando ```refreshenv```.
5. Listo para usar Maven.

**Instalación de Git**
1. Abre una linea de comandos como administrador.
2. Copia y pega en terminal lo siguiente: ```choco install git -y```
3. Espera a que se completen los comandos. 
4. Si no vez algún error ejecuta el comando ```refreshenv```.
5. Listo para usar Git.

**MAC**
**Instalación de Homebrew**
Visita la página https://docs.brew.sh/Installation y sigue los pasos de instalación.

**Instalación de Java**
1. Abre una terminal.
2. Copia,pega y ejecuta en terminal.
```
brew update
brew install java
```
3. Espera a que se completen los comandos. 
4. Si no vez algún error estás listo para usar Java.

**Instalación de Maven**
1. Abre una terminal.
2. Copia,pega y ejecuta en terminal.
```
brew update
brew install maven
```
3. Espera a que se completen los comandos. 
4. Si no vez algún error estás listo para usar Maven.

**Instalación de Git**
1. Abre una terminal.
2. Copia,pega y ejecuta en terminal.
```
brew update
brew install git
```
3. Espera a que se completen los comandos. 
4. Si no vez algún error estás listo para usar Git.

## **3. Estructura del Proyecto**

Dentro de src >main >java >unidaduno; encontrarás la clase ```Empleado``` donde se encuentran todos los atributos y métodos (getters y setter).

En el folder src >test >java>unidaduno, se encuentra la clase ```PruebaEmpleado``` que incluye el método ```main()``` para ejecutar pruebas.

**Directorio**

    ├── src                                   # Archivos fuente
    │   ├── main                       
    |       ├── java
    |           ├── unidaduno
    |               ├── Empleado.java         # Clase con atributos y métodos para empleados 
    |   ├── test                              # Clases para pruebas
    |       ├── java
    |           ├── unidaduno
    |               ├── PruebaEmpleado.java   # Clase para pruebas 
    ├── .gitignore                            # Ignorar archivos              
    ├── pom.xml                               # Archivo con dependencias
    └── README.md

## **4. Ejecución del Proyecto y Resultados**

1. Abre una terminal o linea de comandos.
2. Clona el repositorio copiando, pegando y ejecutando lo siguiente:  
```git clone https://gsanchezm@bitbucket.org/msc_davinci/pb_unidad1.git```.
3. Abre tu IDE  
4. Todas las dependencias deben ser descargadas
5. Ejecuta la clase ```PruebaEmpleado```.
6. Aparecerá el mensaje de bienvenida y te preguntará por el número de empleados a ingresar.
```
****************** BIENVENIDO AL SISTEMA DE CÁCULO SALARIAL DE EMPLEADOS **************************

¿Cuántos empleados quiere ingresar? (debe ser un número mayor a 0):
```
7. Ingresa el número de empleados que necesites.

## **5. Casos de Uso y Datos de Prueba**

**Pruebas para comprobar el comportamiento ideal**

1. Ingresa el número de empleados: ```2```.
2. Escribe el nombre: ```Gilberto```.
3. Escribe el apellido: ```Sánchez```
4. Escribe el salario mensual: ```10,000```
5. Deberás ver el resumen para el primer empleado cómo el que se muestra:
```
Resumen para empleado 1: 
Su nombre es: Gilberto
Su apellido es: Sánchez
Su salario mensual es: 10000.000000
Su salario anual sin incremento es: 120000.000000
Su salario anual con incremento es: 132000.000000
```
6. Repite los pasos 2 - 6 con los siguientes datos: Pedro, Pérez, 15,000
7. Deberás ver el resumen para el primer empleado cómo el que se muestra:
```
Resumen para empleado 2: 
Su nombre es: Pedro
Su apellido es: Pérez
Su salario mensual es: 15000.000000
Su salario anual sin incremento es: 180000.000000
Su salario anual con incremento es: 198000.000000
```

**Pruebas para generar errores**

_1. El número de empleados es un string_
```
¿Cuántos empleados quiere ingresar? (debe ser un número mayor a 0): string
"string" no es un número.
```
_2. El número de empleados es 0_
```
¿Cuántos empleados quiere ingresar? (debe ser un número mayor a 0): 0
¿Cuántos empleados quiere ingresar? (debe ser un número mayor a 0):
```
_3.nombre y apellido vacíos_
```
Escriba su nombre: 

Escriba su nombre: Gil
Escriba su apellido: 
Escriba su apellido: 
Escriba su apellido: Sanchez
```
_4.El salario mensual es un string_
```
Escriba su salario mensual, recuerda que debe ser un número positivo y mayor a 0: text
"text" no es un número.
```
_5. El salario mensual es negativo_
```
Escriba su salario mensual: -1
Escriba su salario mensual, recuerda que debe ser un número positivo y mayor a 0: 
```

_6. El salario mensual es 0_

```
Escriba su salario mensual, recuerda que debe ser un número positivo y mayor a 0: 0
Escriba su salario mensual, recuerda que debe ser un número positivo y mayor a 0: 
```